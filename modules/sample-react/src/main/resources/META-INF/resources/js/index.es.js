import React from 'react';

import ReactDOM from 'react-dom';

class MyComponent extends React.Component {
	constructor(props) {
		super(props);
		  this.state = {
				  fileEntryId: '35115'
		  }
		  this.handleChange = this.handleChange.bind(this);
		  this.getDocument = this.getDocument.bind(this);

	}
	
  render() {
    return (
    	<div>
    		<input type="number" value={this.state.fileEntryId} name="fileEntryId" onChange={ this.handleChange } />
    	    <button type="button" onClick={ this.getDocument } >Get Document</button>
    	</div>
    );
  }
  
  getDocument (e) {
	  document.getElementById('displayDocumentData').innerHTML = ""
	  console.log("Props Value = " + this.state.fileEntryId);
	  const apiUrl = '/o/test/v1.0/document/' + this.state.fileEntryId;
	  /*fetch(apiUrl, {
		  method: 'GET',
		  withCredentials: true,
		  credentials: 'include',
		  headers: new Headers({
			  'Authorization': 'Basic '+btoa('test@liferay.com:test')
		  })
	  })
	  .then((response) => response.json())
	  .then((data) => console.log('This is your data', data)); */
	  
	  
	  // Error Handling
	  
	  fetch(apiUrl, {
		  method: 'GET',
		  withCredentials: true,
		  credentials: 'include',
		  headers: new Headers({
			  'Authorization': 'Basic '+btoa('test@liferay.com:test')
		  })
	  })
	  .then(response => {
	    if (response.ok) {
	      return response.json()
	    } else {
	      console.log(response.status)
	      document.getElementById('displayDocumentData').innerHTML = "some error happend maybe 404"
	      return Promise.reject('some error happend maybe 404')
	    }
	  })
	  .then(data => {
		  console.log('Reponse Data = ', data)
		  document.getElementById('displayDocumentData').innerHTML = "<b>Title :</b> " + data.title;
	  })
	  .catch(error => console.log('error is', error));
  }
  
  handleChange (e) {
	  this.setState({fileEntryId: e.target.value});
  }
}
export default function(elementId) {
	ReactDOM.render(<MyComponent />, document.getElementById(elementId));
	
}

