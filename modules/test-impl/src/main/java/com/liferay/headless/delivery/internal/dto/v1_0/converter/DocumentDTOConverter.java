package com.liferay.headless.delivery.internal.dto.v1_0.converter;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.vulcan.dto.converter.DTOConverter;
import com.liferay.portal.vulcan.dto.converter.DTOConverterContext;
import com.liferay.test.dto.v1_0.Document;

import org.osgi.service.component.annotations.Reference;

public class DocumentDTOConverter
	implements DTOConverter<DLFileEntry, Document> {

	@Override
	public String getContentType() {
		return Document.class.getSimpleName();
	}
	
	@Override
	public Document toDTO(DTOConverterContext dtoConverterContext)
		throws Exception {

		FileEntry fileEntry = _dlAppService.getFileEntry(
			(Long)dtoConverterContext.getId());

		return new Document() {
			{
				dateCreated = fileEntry.getCreateDate();
				dateModified = fileEntry.getModifiedDate();
				description = fileEntry.getDescription();
				documentFolderId = fileEntry.getFolderId();
				encodingFormat = fileEntry.getMimeType();
				fileExtension = fileEntry.getExtension();
				id = fileEntry.getFileEntryId();
				sizeInBytes = fileEntry.getSize();
				title = fileEntry.getTitle();
			}
		};
	}
	
	@Reference
	private DLAppService _dlAppService;
}
