package com.liferay.test.internal.resource.v1_0;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFileEntryType;
import com.liferay.document.library.kernel.model.DLVersionNumberIncrease;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.document.library.kernel.service.DLFileEntryTypeLocalService;
import com.liferay.dynamic.data.mapping.kernel.DDMFormValues;
import com.liferay.dynamic.data.mapping.kernel.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureService;
import com.liferay.dynamic.data.mapping.util.DDMBeanTranslator;
import com.liferay.headless.delivery.internal.dto.v1_0.converter.DocumentDTOConverter;
import com.liferay.headless.delivery.internal.dto.v1_0.util.CustomFieldsUtil;
import com.liferay.headless.delivery.internal.dto.v1_0.util.DDMFormValuesUtil;
import com.liferay.headless.delivery.internal.dto.v1_0.util.ServiceContextUtil;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.vulcan.dto.converter.DTOConverterRegistry;
import com.liferay.portal.vulcan.multipart.BinaryFile;
import com.liferay.portal.vulcan.multipart.MultipartBody;
import com.liferay.test.dto.v1_0.ContentField;
import com.liferay.test.dto.v1_0.CustomField;
import com.liferay.test.dto.v1_0.Document;
import com.liferay.test.dto.v1_0.DocumentType;
import com.liferay.test.resource.v1_0.EntityResource;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * @author Javier Gamarra
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/entity.properties",
	scope = ServiceScope.PROTOTYPE, service = EntityResource.class
)
public class EntityResourceImpl extends BaseEntityResourceImpl {
	
	@Override
	public Document postDocument(@NotNull Long siteId, MultipartBody multipartBody) throws Exception {
		System.out.println("Site Id === " + siteId);
		return _addDocument(siteId, 0L, siteId, multipartBody);
	}
	
	@Override
	public void deleteDocument(@NotNull Long documentId) throws Exception {
		_dlAppService.deleteFileEntry(documentId);
	}
	
	@Override
	public Document getDocument(Integer documentId) throws Exception {
		System.out.println("Get Document id = " + documentId);
		return _toDocument(_dlAppService.getFileEntry(documentId));
	}
	
	private Document _toDocument(FileEntry fileEntry) throws Exception {
		
		return new Document() {
			{
				dateCreated = fileEntry.getCreateDate();
				dateModified = fileEntry.getModifiedDate();
				description = fileEntry.getDescription();
				documentFolderId = fileEntry.getFolderId();
				encodingFormat = fileEntry.getMimeType();
				fileExtension = fileEntry.getExtension();
				id = fileEntry.getFileEntryId();
				sizeInBytes = fileEntry.getSize();
				title = fileEntry.getTitle();
			}
		};
	}
	
	@Override
	public Document putDocument(@NotNull Long documentId, MultipartBody multipartBody) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Put Document called ..");
		

		Optional<Document> documentOptional =
			multipartBody.getValueAsInstanceOptional(
				"document", Document.class);

		if ((multipartBody.getBinaryFile("file") == null) &&
			!documentOptional.isPresent()) {

			System.out.println("Error ====>>> No document or file found in body");
			throw new BadRequestException("No document or file found in body");
		}

		FileEntry existingFileEntry = _dlAppService.getFileEntry(documentId);

		BinaryFile binaryFile = Optional.ofNullable(
			multipartBody.getBinaryFile("file")
		).orElse(
			new BinaryFile(
				existingFileEntry.getMimeType(),
				existingFileEntry.getFileName(),
				existingFileEntry.getContentStream(),
				existingFileEntry.getSize())
		);

		existingFileEntry = _moveDocument(
			documentId, documentOptional, existingFileEntry);

		return _toDocument(
			_dlAppService.updateFileEntry(
				documentId, binaryFile.getFileName(),
				binaryFile.getContentType(),
				documentOptional.map(
					Document::getTitle
				).orElse(
					existingFileEntry.getTitle()
				),
				documentOptional.map(
					Document::getDescription
				).orElse(
					null
				),
				null, DLVersionNumberIncrease.AUTOMATIC,
				binaryFile.getInputStream(), binaryFile.getSize(),
				_getServiceContext(
					() -> new Long[0], () -> new String[0],
					existingFileEntry.getFolderId(), documentOptional,
					existingFileEntry.getGroupId())));
	}
	
	private FileEntry _moveDocument(
			Long documentId, Optional<Document> documentOptional,
			FileEntry existingFileEntry)
		throws Exception {

		Optional<Long> folderIdOptional = documentOptional.map(
			Document::getDocumentFolderId
		).filter(
			folderId -> folderId != existingFileEntry.getFolderId()
		);

		if (folderIdOptional.isPresent()) {
			return _dlAppService.moveFileEntry(
				documentId, folderIdOptional.get(), new ServiceContext());
		}

		return existingFileEntry;
	}
	
	private Document _addDocument(
			Long repositoryId, long documentFolderId, Long groupId,
			MultipartBody multipartBody)
		throws Exception {

		BinaryFile binaryFile = multipartBody.getBinaryFile("file");

		if (binaryFile == null) {
			throw new BadRequestException("No file found in body");
		}

		Optional<Document> documentOptional =
			multipartBody.getValueAsInstanceOptional(
				"document", Document.class);

		return _toDocument(
			_dlAppService.addFileEntry(
				repositoryId, documentFolderId, binaryFile.getFileName(),
				binaryFile.getContentType(),
				documentOptional.map(
					Document::getTitle
				).orElse(
					binaryFile.getFileName()
				),
				documentOptional.map(
					Document::getDescription
				).orElse(
					null
				),
				null, binaryFile.getInputStream(), binaryFile.getSize(),
				_getServiceContext(
					() -> new Long[0], () -> new String[0], documentFolderId,
					documentOptional, groupId)));
	}
	
	private ServiceContext _getServiceContext(
			Supplier<Long[]> defaultCategoriesSupplier,
			Supplier<String[]> defaultKeywordsSupplier, Long documentFolderId,
			Optional<Document> documentOptional, Long groupId)
		throws Exception {

		ServiceContext serviceContext = ServiceContextUtil.createServiceContext(
			documentOptional.map(
				Document::getTaxonomyCategoryIds
			).orElseGet(
				defaultCategoriesSupplier
			),
			documentOptional.map(
				Document::getKeywords
			).orElseGet(
				defaultKeywordsSupplier
			),
			_getExpandoBridgeAttributes1(documentOptional), groupId,
			documentOptional.map(
				Document::getViewableByAsString
			).orElse(
				Document.ViewableBy.OWNER.getValue()
			));

		serviceContext.setUserId(contextUser.getUserId());

		Optional<DLFileEntryType> dlFileEntryTypeOptional =
			_getDLFileEntryTypeOptional(
				documentFolderId, documentOptional, groupId);

		if (dlFileEntryTypeOptional.isPresent()) {
			DLFileEntryType dlFileEntryType = dlFileEntryTypeOptional.get();

			serviceContext.setAttribute(
				"fileEntryTypeId", dlFileEntryType.getFileEntryTypeId());

			Document document = documentOptional.get();

			List<DDMStructure> ddmStructures =
				dlFileEntryType.getDDMStructures();

			DocumentType documentType = document.getDocumentType();

			ContentField[] contentFields = documentType.getContentFields();

			for (DDMStructure ddmStructure : ddmStructures) {
				com.liferay.dynamic.data.mapping.model.DDMStructure
					modelDDMStructure = _ddmStructureService.getStructure(
						ddmStructure.getStructureId());

				com.liferay.dynamic.data.mapping.storage.DDMFormValues
					ddmFormValues = DDMFormValuesUtil.toDDMFormValues(
						contentFields, modelDDMStructure.getDDMForm(),
						_dlAppService, groupId, _journalArticleService,
						_layoutLocalService,
						contextAcceptLanguage.getPreferredLocale(),
						transform(
							ddmStructure.getRootFieldNames(),
							modelDDMStructure::getDDMFormField));

				serviceContext.setAttribute(
					DDMFormValues.class.getName() + StringPool.POUND +
						ddmStructure.getStructureId(),
					_ddmBeanTranslator.translate(ddmFormValues));
			}
		}

		return serviceContext;
	}
	
	private Map<String, Serializable> _getExpandoBridgeAttributes1(
		Optional<Document> documentOptional) {

		return CustomFieldsUtil.toMap(
			DLFileEntry.class.getName(), contextCompany.getCompanyId(),
			_getExpandoBridgeAttributes(documentOptional),
			contextAcceptLanguage.getPreferredLocale());
	}
	
	private CustomField[] _getExpandoBridgeAttributes(
			Optional<Document> documentOptional) {

			return documentOptional.map(
				Document::getCustomFields
			).orElse(
				null
			);
		}

	
	private Optional<DLFileEntryType> _getDLFileEntryTypeOptional(
			long documentFolderId, Optional<Document> documentOptional,
			Long groupId) {

			return documentOptional.map(
				Document::getDocumentType
			).map(
				DocumentType::getName
			).map(
				name -> {
					try {
						for (DLFileEntryType dlFileEntryType :
								_dlFileEntryTypeLocalService.
									getFolderFileEntryTypes(
										new long[] {groupId}, documentFolderId,
										true)) {

							if (name.equals(
									dlFileEntryType.getName(
										contextAcceptLanguage.
											getPreferredLocale()))) {

								return dlFileEntryType;
							}
						}
					}
					catch (Exception exception) {
						if (_log.isDebugEnabled()) {
							_log.debug(exception, exception);
						}
					}

					return null;
				}
			);
		}

	private static final Log _log = LogFactoryUtil.getLog(
			EntityResourceImpl.class);
	
	@Reference
	private DocumentDTOConverter _documentDTOConverter;
	
	@Reference
	private DTOConverterRegistry _dtoConverterRegistry;
	
	@Reference
	private DDMStructureService _ddmStructureService;
	
	@Reference
	private DDMBeanTranslator _ddmBeanTranslator;
	
	@Reference
	private DLAppService _dlAppService;
	
	@Reference
	private JournalArticleService _journalArticleService;
	
	@Reference
	private LayoutLocalService _layoutLocalService;
	
	@Reference
	private DLFileEntryTypeLocalService _dlFileEntryTypeLocalService;

}