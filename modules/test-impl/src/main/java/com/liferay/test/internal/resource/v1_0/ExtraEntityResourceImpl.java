package com.liferay.test.internal.resource.v1_0;

import com.liferay.test.resource.v1_0.ExtraEntityResource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * @author Javier Gamarra
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/extra-entity.properties",
	scope = ServiceScope.PROTOTYPE, service = ExtraEntityResource.class
)
public class ExtraEntityResourceImpl extends BaseExtraEntityResourceImpl {
}